var Facebook         = require('facebook');
Facebook.appid       = '656924857709089';
Facebook.permissions = ['publish_stream'];

var args = arguments[0] || {};

$.lost_window.orientationModes = [Ti.UI.PORTRAIT];

// Wait 1 second to prevent miss clicks
setTimeout(function(){
    $.try_again.addEventListener('click', function(){
        $.lost_window.close();
    });
    $.share.addEventListener('click', function(){
        shareFacebook();
    });
    if(Ti.Platform.osname !== 'android'){
        $.menu.addEventListener('click', function(){
            var menu_window = Alloy.createController('index').getView();
            menu_window.open();
            $.lost_window.close();
        });
    }
}, 1000);

var pointsListener = function(data){
    $.points.text = 0;
    var points    = 0;

    var timeout = function(){
        setTimeout(function(){
            if($.points.text != data.points){
                if( data.points > 1000 && $.points.text < data.points && (data.points-$.points.text > 100) ){
                    $.points.text = points += 100;
                } else {
                    $.points.text = points++;
                }
                timeout();
            }
        }, 3);
    };
    timeout();

    setTimeout(function(){
        if(data.points === 0){
            return;
        }
        var dialog;
        if(Ti.Platform.osname === 'android') {
            var name = Ti.UI.createTextField();
            var last_name = Ti.App.Properties.getString('name');
            if(last_name){
                name.value = last_name;
            }
            dialog  = Ti.UI.createOptionDialog({
                title: L('enter_name', 'Enter your name'),
                androidView: name,
                buttonNames:[ L('cancel', 'Cancel'), L('send_score', 'Send Score')]
            });

            dialog.addEventListener('click', function(e) {
                if (e.index == 1) {
                    Ti.App.Properties.setInt('user_record', data.points);
                    Ti.App.Properties.setString('name', name.value);
                    postScore(name.value, $.points.text);
                    name.value = '';
                }
            });

        } else {
            dialog = Ti.UI.createAlertDialog({
                title: L('enter_name', 'Enter your name'),
                style: Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,
                buttonNames: ['Cancel', 'Send Score']
            });
            // dialog.text = Ti.App.Properties.getString('name');
            dialog.addEventListener('click', function(e){
                if(e.index === 1 && e.text){
                    Ti.App.Properties.setString('name', e.text);
                    postScore(e.text, $.points.text);
                    Ti.App.Properties.setInt('user_record', data.points);
                }
            });
        }

        var user_record = Ti.App.Properties.getInt('user_record', 0);
        if (data.points > user_record) {
            dialog.show();
        }
    }, 800);
};

$.lost_window.addEventListener('points', pointsListener);

// $.lost_window.addEventListener('close', function(){
//     Ti.App.removeEventListener('points', pointsListener);
// });

function postScore(name, points){
    var client = Ti.Network.createHTTPClient();
    client.open('POST', 'https://api.mongolab.com/api/1/databases/tapthecolor/collections/scores?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe');
    client.setRequestHeader('Content-Type', 'application/json');
    client.send( JSON.stringify({ name: name, points: points }) );
}

$.lost_window.addEventListener('android:back', function(e) {
    e.cancelBubble  = true;
    var menu_window = Alloy.createController('index').getView();
    menu_window.open();
    $.lost_window.close();
});

function shareFacebook(){
    if(Facebook.loggedIn) {
        send_facebook_stream();
    } else {
        Facebook.authorize();
    }

}

Facebook.addEventListener('login',function(e) {
    Ti.API.info('Facebook login data' + JSON.stringify(e.data));
    send_facebook_stream();
});

function send_facebook_stream(){

    var data = {
        name        : 'Tap The Color',
        link        : 'https://play.google.com/store/apps/details?id=com.gavalos.tapcolor',
        caption     : L('social_caption'),
        description : L('i_made') + ' ' + $.points.text + ' '+ L('points') +'!'
    };

    facebook_dialog = Facebook.dialog('feed', data, showRequestResult);

    function showRequestResult(r){
        if (r.result){
            facebook_response = Ti.UI.createAlertDialog({
                title: 'Facebook Shared!',
                message: 'Your score has been published.'
            });
            facebook_response.show();
        } else {
            // facebook_response = Ti.UI.createAlertDialog({
            //     title: 'Facebook Stream was cancelled',
            //     message: 'Nothing was published.'
            // });

        }
    }

}

// Animate buttons
var animation = Ti.UI.createAnimation({
    top      : 0,
    opacity  : 1,
    duration : 1000,
    curve    : Ti.UI.ANIMATION_CURVE_EASE_OUT
});

$.buttons.animate(animation);