function startGame(e) {
    var gameWindow = Alloy.createController('game').getView();
    gameWindow.open();
}

function ranking(e) {
    var rankingWindow = Alloy.createController('ranking').getView();
    rankingWindow.open({ modal: true });
}

var about_dialog = Ti.UI.createAlertDialog({
    message : 'Tap The Color is a game designed and developed by Gilberto Avalos, for issues or comments: avalosagnostic@gmail.com',
    ok      : 'Close',
    title   : 'About'
});

function about(e) {
    about_dialog.show();
}

function exitConfirm(){
    var dialog = Ti.UI.createAlertDialog({
        cancel: 1,
        buttonNames: [L('yes'), 'No'],
        message: L('exit_confirmation'),
        title: 'Confirmation'
      });
      dialog.addEventListener('click', function(e){
        if (e.index === 0){
            if(Ti.Platform.osname === 'android'){
                var activity = Ti.Android.currentActivity;
                activity.finish();
            } else {
                $.index.close();
            }
        }
      });
      dialog.show();
}

$.index.orientationModes = [Titanium.UI.PORTRAIT];

$.index.addEventListener('android:back', function(e) {
    e.cancelBubble = true;
    exitConfirm();
});

$.index.open();