var args = arguments[0] || {};

// Free version
if(Alloy.Globals.version === 'free'){
  var Admob = require('ti.admob');
  var adMobView;
  if(Ti.Platform.osname === 'android'){
    adMobView = Admob.createView({
      publisherId          : "ca-app-pub-3680296853403866/6440987534",
      testing              :false,
      //top                : 10, //optional
      //left               : 0, // optional
      //right              : 0, // optional
      top                  : 0, // optional
      adBackgroundColor    :"#FFFFFF", // optional
      backgroundColorTop   : "#FFFFFF", //optional - Gradient background color at top
      borderColor          : "#CCCCCC", // optional - Border color
      textColor            : "#999999", // optional - Text color
      urlColor             : "#330033", // optional - URL color
      linkColor            : "#330033" //optional -  Link text color
      //primaryTextColor   : "blue", // deprecated -- now maps to textColor
      //secondaryTextColor : "green" // deprecated -- now maps to linkColor
    });
  } else {
    adMobView = Admob.createView({
      width             : '100%',
      height            : 48,
      publisherId       : 'ca-app-pub-3680296853403866/6440987534',
      adBackgroundColor : '#FFFFFF',
      testing           : false,
      keywords          : 'game, games, free games, puzzle'
    });
    $.ads.height = 48;
  }
  $.ads.add(adMobView);
}

function isOS7() {
  // iOS-specific test
  if (Titanium.Platform.name == 'iPhone OS')
  {
    var version = Titanium.Platform.version.split(".");
    var major = parseInt(version[0],10);

    // Can only test this support on a 3.2+ device
    if (major >= 7)
    {
      return true;
    }
  }
  return false;
}

$.game_window.orientationModes = [Titanium.UI.PORTRAIT];

var iOS7          = isOS7();
$.game_window.top = iOS7 ? 20 : 0;

// Sounds
var successSound = Titanium.Media.createSound({
  url: Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'sounds/success.wav').nativePath,
  preload: true
});

var failSound = Titanium.Media.createSound({
  url: Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'sounds/failed.wav').nativePath,
  preload: true
});

var gameOverSound = Titanium.Media.createSound({
  url: Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, 'sounds/game_over.wav').nativePath,
  preload: true
});

// Colors
var colors = [
  { name : 'Green', color: '#2ecc71' },
  { name : 'Blue', color: '#3498db' },
  { name : 'Yellow', color: '#f1c40f' },
  { name : 'Red', color: '#e74c3c' }
];

// Global vars
var points, lives, counter_duration, counter, selectedColor, been_lost = false;

var getRandomColor = function(){
  var _colors = _.filter(colors, function(color){ return !color.selected_name; });
  var color = _colors[ Math.round( Math.random() * (_colors.length-1) ) ];
  color.selected_name = true;
  return color;
};

var getRandomBackground = function(){
  var _colors = _.filter(colors, function(color){ return !color.selected_background; });
  var color = _colors[ Math.round( Math.random() * (_colors.length-1) ) ];
  color.selected_background = true;
  return color;
};

var generateColors = function(){
  counter      = 3;
  $.timer.text = counter;

  _.each(colors, function(color){
    color.selected_background = false;
    color.selected_name       = false;
  });

  selectedColor          = colors[ Math.round( Math.random() * (colors.length-1) ) ];
  $.objective_value.text = L(selectedColor.name);

  var otherColors = _.filter(colors, function(color){
    return color.name !== selectedColor.name;
  });

  $.objective_value.color = otherColors[ Math.round( Math.random() * (otherColors.length-1) ) ].color;

  // Buttons
  for(var i = 1; i <= 4; i++){
    var randomName                 = getRandomColor();
    var randomBackground           = getRandomBackground();
    $['button_'+i].backgroundColor = randomBackground.color;
    $['button_'+i].title           = L(randomName.name);
  }
};

var timeout;
var timer = function(){
  if(lives < 1){ return; }
  timeout = setTimeout(function(){

    if(counter > 0){
      counter = counter - 1;
      $.timer.text = counter;
      resetTimer();
    } else {
      if(lives > 0){
        $['heart_'+lives].image = '/images/heart.png';
        lives = lives - 1;
        failSound.reset();
        failSound.play();
      }
      // $.lives.text = L('lives') + ' ' + lives;
      if(lives < 1){
        lost();
      } else {
        generateColors();
        resetTimer();
      }
    }

  }, counter_duration);
};

var resetTimer = function(clear){
  if(timeout){
    clearTimeout(timeout);
  }
  if(!clear){
    timer();
  }
};

// Game Logic
var startGame = function(){
  setTimeout(function() {

    // Default values
    points           = 0;
    lives            = 3;
    counter_duration = 1000;
    counter          = 3;
    been_lost        = false;

    // Default counter
    $.timer.text  = counter;
    $.points.text = points + ' pts';
    // $.lives.text  = L('lives') + ' ' + lives;

    // Generate Colors
    generateColors();

    // Call Timer
    resetTimer();

  }, 500);
};

// Event listeners
for(var i = 1; i <= 4; i++){
  $['button_'+i].addEventListener('click', function(e){
    var button = e.source;

    // Correct
    var correctColor = _.find(colors, function(color){
      return color.name === selectedColor.name;
    });

    if( button.backgroundColor === correctColor.color ){
      counter_duration = counter_duration - 15;
      successSound.reset();
      successSound.play();
      points += 100 + counter;
      $.points.text = points + ' pts';

    // Incorrect
    } else {
      failSound.reset();
      failSound.play();
      if(lives > 0){
        $['heart_'+lives].image = '/images/heart.png';
        lives = lives - 1;
      }
      // $.lives.text = L('lives') + ' ' + lives;
      if(lives < 1){
        lost();
      }
    }

    if(lives > 0){
      generateColors();
    }

    resetTimer();
  });
}

var opened_lost = false;
var readed_instructions = Ti.App.Properties.getBool('readed_instructions');
$.game_window.addEventListener('focus', function(){
  for(var i = 1; i <= 3; i++){
    $['heart_'+i].image = '/images/heart_red.png';
  }
  opened_lost = false;

  if (readed_instructions) {
    startGame();
  } else {
    $.instructions.height = '100%';
    setTimeout(function() { 
      $.instructions.visible = true;
    }, 0);
  }
});

function startByInstructions() {
  $.instructions.hide();
  $.instructions.height = 0;
  readed_instructions = true;
  Ti.App.Properties.getBool('readed_instructions', true);
  startGame();
}

var lost = function(){
  if(been_lost) return;
  if(!opened_lost){
    gameOverSound.play();
    opened_lost = true;
    var lostWindow = Alloy.createController('lost').getView();
    lostWindow.open();
    lostWindow.fireEvent('points', { points: points });
  }
  been_lost = true;
};

$.game_window.addEventListener('blur', function(e) {
  resetTimer(true);
});

// startGame();

// Instructions text
$.label_start.text = L('label_start');
$.label_example.text = L('Green');
$.label_block.text = L('Red');
$.instructions_title.text = L('instructions_title');
$.instructions_content.text = L('instructions_content');