var args = arguments[0] || {};

var style;
if (Ti.Platform.name === 'android'){
  style = Ti.UI.ActivityIndicatorStyle.DARK;
} else {
  style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
}

var client = Ti.Network.createHTTPClient({
    onload : function(e) {
      $.scores.remove($.loading);
      var scores = [];
      try{
        scores = JSON.parse(this.responseText);
      } catch(e) {}
      addScores(scores);
    }
});
client.open('GET', 'https://api.mongolab.com/api/1/databases/tapthecolor/collections/scores?apiKey=sAsd-LJVmrcTa_1-b_nL5SaDkuScCfoe&s={points:-1}&l=100');
client.send();

var addScores = function(scores){
  scores.forEach(function(score){

    var score_view = Ti.UI.createView();
    $.addClass(score_view, 'score');

    var name_label = Ti.UI.createLabel({ text: score.name });
    $.addClass(name_label, 'name');

    var score_value = Ti.UI.createLabel({ text: score.points });
    $.addClass(score_value, 'score_value');

    score_view.add(name_label);
    score_view.add(score_value);

    $.scores.add(score_view);
  });
};

function closeWindow(){
  $.ranking_window.close();
}